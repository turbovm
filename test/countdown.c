#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#define TVM_BYTE uint8_t
#define TVM_IMM_T uint16_t
#define TVM_IMMS_T int16_t
#define TVM_WORD int32_t
#define TVM_WORDSIZE 32

#define TVM_REG(N) reg[N]

struct turbovm {
  TVM_WORD reg[256];
  TVM_WORD instr;
};

static void syscall(struct turbovm *vm, TVM_IMM_T num) {
  TVM_WORD *reg = vm->reg;
  switch(num) {
  case 1:
    exit(TVM_REG(3) & 0xff);
    break;
  case 2:
    printf("65536\n", TVM_REG(3));
    break;
  default:
    fprintf(stderr, "Illegal syscall: %u\n", num);
    exit(1);
  }
}

struct turbovm *run(struct turbovm *vm) {
  TVM_WORD *reg = vm->reg;
  while(1) {
    switch(vm->instr) {
    case          0:  
        L0000000000:  TVM_REG(3) = 0x5f5 << 16;
    case          4:  
        L0000000004:  TVM_REG(3) |= (TVM_IMM_T) 0xe100;
    case          8:  
        L0000000008:  if(TVM_REG(0) == TVM_REG(3)) {
    case         12:  
        L0000000012:  goto L0000000024;
                      }
    case         16:  
        L0000000016:  TVM_REG(3) -= (TVM_IMM_T) 0x1;
    case         20:  
        L0000000020:  goto L0000000008;
    case         24:  
        L0000000024:  TVM_REG(3) = 0x0;
    case         28:  
        L0000000028:  vm->instr = (TVM_WORD) 32; syscall(vm, 0x1);
    default:
        /* whoops! value out of range! */
        fprintf(stderr, "Invalid instruction pointer: %u\n", vm->instr);        exit(0x55);
        break;
    }
  }
}

int main(int argc, char **argv) {
  struct turbovm vm;
  memset(&(vm.reg), 0, sizeof(vm.reg));
  vm.instr = 0;
  run(&vm);
  /* Should not be reached. */
  return 1;
}
