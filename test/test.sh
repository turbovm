#! /bin/sh

# Extract expected output
awk '$2 == "EXPECT" { print $3 }' < test.tvma > test.expected

# Assemble test program
../src/turbovm-asm < test.tvma > test.tvm

# Run test program
../src/turbovm < test.tvm > test.out

# Compare expected and obtained outputs
if cmp -s test.out test.expected
then
	echo "Test passed"
else
	echo "Expected and obtained output differ."
	echo "Run diff -u test.out test.expected to see differences."
	exit 1
fi
