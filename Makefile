SUBDIRS = doc src test

build :
	cd src && $(MAKE)

doc :
	cd doc && $(MAKE)

test : build
	cd test && $(MAKE) test

all :
	for dir in $(SUBDIRS); do (cd "$$dir" && $(MAKE) all); done

clean :
	for dir in $(SUBDIRS); do (cd "$$dir" && $(MAKE) clean); done

distclean :
	for dir in $(SUBDIRS); do (cd "$$dir" && $(MAKE) distclean); done

.PHONY : all build clean distclean doc test
