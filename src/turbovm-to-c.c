#include "turbovm.h"
#include "opcodes.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BYTE uint8_t
#define IMM_T uint16_t
#define IMMS_T int16_t
#define WORD TVM_WORD

#define OPCODE code[p]
#define REG(N) N
#define REG1 REG(code[p + 1])
#define REG2 REG(code[p + 2])
#define REG3 REG(code[p + 3])
#define IMM (((IMM_T) (code[p + 2] << 8)) | ((IMM_T) code[p + 3]))
#define IMMS (((IMMS_T) (code[p + 2] << 8)) | ((IMMS_T) code[p + 3]))
#define MATCH(OP) case TVM_OP_ ## OP:
#define EMIT(FMT) fprintf(stream, FMT, REG1, REG2, REG3, IMM, IMMS)
#define IMPL(OP, FMT) case TVM_OP_ ## OP: EMIT(FMT); NEXT
#define IF(OP, SYM) case TVM_OP_ ## OP: fprintf(stream, "if(TVM_REG(%1$u) " #SYM " TVM_REG(%2$u)) {\n", REG1, REG2); \
                                        *pptr = p + 4; \
                                        emit_instruction(stream, code, size, pptr); \
                                        fprintf(stream, "                      }\n"); \
                                        NEXT
#define BINOP(OP, SYM) IMPL(OP, "TVM_REG(%1$u) = TVM_REG(%2$u) " #SYM "TVM_REG(%3$u);\n")
#define BINOPI(OP, SYM) IMPL(OP, "TVM_REG(%1$u) " #SYM "= (TVM_IMM_T) 0x%4$x;\n")
#define BINOPIS(OP, SYM) IMPL(OP, "TVM_REG(%1$u) " #SYM "= (TVM_IMMS_T) 0x%5$x;\n")
#define NEXT break

struct turbovm {
  WORD reg[256];
  BYTE *code;
  WORD code_size;
};

static void emit_instruction(FILE *stream, BYTE *code, WORD size, WORD *pptr) {
  WORD p = *pptr;
  fprintf(stream, "    case %1$10u:  \n        L%1$010u:  ", p);
  switch(OPCODE) {
    BINOP(AND, &);
    BINOP(OR, |);
    BINOP(XOR, ^);
    IMPL(SH, "TVM_REG(%1$u) = (TVM_REG(%3$u) < 0) "
	 "? (TVM_REG(%2$u) >> -TVM_REG(%3$u)) "
	 ": (TVM_REG(%2$u) << TVM_REG(%3$u));\n");
    IMPL(SHS, "TVM_REG(%1$u) = (TVM_REG(%3$u) < 0) "
	 "? (TVM_REG(%2$u) >> -TVM_REG(%3$u)) "
	 ": (TVM_REG(%2$u) << TVM_REG(%3$u));\n");
    IMPL(ROT, "TVM_REG(%1$u) = (TVM_REG(%3$u) < 0) "
	 "? ((TVM_REG(%2$u) >> -TVM_REG(%3$u)) | (TVM_REG(%2$u) << (TVM_WORDSIZE + TVM_REG(%3$u))))"
	 ": ((TVM_REG(%2$u) << TVM_REG(%3$u)) | (TVM_REG(%2$u) >> (TVM_WORDSIZE - TVM_REG(%3$u))));\n");
    IMPL(LB, "TVM_REG(%1$u) = (TVM_WORD) *((BYTE *) (TWM_REG[%2$u) + TVM_REG(%3$u)))\n");
    IMPL(LR, "TVM_REG(%1$u) = TVM_REG(%2$u)\n");
    IMPL(LW, "TVM_REG(%1$u) = *((TVM_WORD *) (TVM_REG(%2$u) + TVM_REG(%3$u)));\n");
    IMPL(STB, "*((TVM_BYTE *) (TVM_REG(%2$u) + TVM_REG(%3$u))) = (TVM_BYTE) (TVM_REG(%1$u) & 0xff);\n");
    IMPL(STW, "*((TVM_WORD *) (TVM_REG(%2$u) + TVM_REG(%3$u))) = TVM_REG(%1$u);\n");

    BINOPI(ANDI, &);
    BINOPI(ORI, |);
    BINOPI(XORI, ^);
    IMPL(SHI, "TVM_REG(%1$u) = (0x%5$x < 0) ? TVM_REG(%2$u) >> -0x%5$x : TVM_REG(%2$u) << 0x%5$x;\n");
    IMPL(ROTI, "TVM_REG(%1$u) = (0x%5$x < 0) "
	 "? ((TVM_REG(%2$u) >> -0x%5$x) | (TVM_REG(%2$u) << (TVM_WORDSIZE + 0x%5$x)))"
	 ": ((TVM_REG(%2$u) << 0x%5$x) | (TVM_REG(%2$u) >> (TVM_WORDSIZE - 0x%5$x)));\n");
    IMPL(LI, "TVM_REG(%1$u) = 0x%4$x;\n");
    IMPL(LUI, "TVM_REG(%1$u) = 0x%4$x << 16;\n");

    BINOP(ADD, +);
    BINOP(DIV, /);
    BINOP(DIVS, /);
    BINOP(MOD, %);
    BINOP(MODS, %);
    BINOP(MUL, *);
    BINOP(MULS, *);
    BINOP(SUB, -);

    BINOPI(ADDI, +);
    BINOPI(DIVI, /);
    BINOPIS(DIVIS, /);
    BINOPI(MODI, %);
    BINOPIS(MODIS, %);
    BINOPI(MULI, *);
    BINOPIS(MULIS, *);
    BINOPI(SUBI, -);

  case TVM_OP_CALL:
    fprintf(stream, "TVM_REG(%u] = (TVM_WORD) %u; tvm->instr = (TVM_BYTE*) TVM_REG(%u]; break;\n",
	    REG1, p + 4, REG2);
    NEXT;
  case TVM_OP_GOTO:
    fprintf(stream, "vm->instr = (TVM_WORD) TVM_REG(%u]; break;\n", REG1);
    NEXT;

  case TVM_OP_CALLI:
    fprintf(stream, "TVM_REG(%u] = (TVM_WORD) %u; goto L%010u;\n",
	    REG1, p + 4, p + IMMS);
    NEXT;
  case TVM_OP_GOTOI:
    fprintf(stream, "goto L%010u;\n", p + IMMS);
    NEXT;
  case TVM_OP_SYS:
    fprintf(stream, "vm->instr = (TVM_WORD) %d; syscall(vm, 0x%x);\n", p + 4, IMM);
    NEXT;

    IF(IFEQ, ==);
    IF(IFGE, >=);
    IF(IFGT, >);
    IF(IFLE, <=);
    IF(IFLT, <);
    IF(IFNE, !=);
  default:
    fprintf(stderr, "Illegal opcode %02x at %10u\n", OPCODE, p);
    exit(255);
  }
}

void compile_to_stream(FILE *stream, BYTE *code, WORD size) {
  WORD p;
  fprintf(stream,
	  "#include <stdio.h>\n"
	  "#include <stdlib.h>\n"
	  "#include <string.h>\n"
	  "#include <inttypes.h>\n"
	  "\n"
	  "#define TVM_BYTE uint8_t\n"
	  "#define TVM_IMM_T uint16_t\n"
	  "#define TVM_IMMS_T int16_t\n"
	  "#define TVM_WORD int32_t\n"
	  "#define TVM_WORDSIZE 32\n\n"
	  "#define TVM_REG(N) reg[N]\n"
	  "\n"
	  "struct turbovm {\n"
	  "  TVM_WORD reg[256];\n"
	  "  TVM_WORD instr;\n"
	  "};\n"
	  "\n"
	  "static void syscall(struct turbovm *vm, TVM_IMM_T num) {\n"
	  "  TVM_WORD *reg = vm->reg;\n"
	  "  switch(num) {\n"
	  "  case 1:\n"
	  "    exit(TVM_REG(3) & 0xff);\n"
	  "    break;\n"
	  "  case 2:\n"
	  "    printf(\"%d\\n\", TVM_REG(3));\n"
	  "    break;\n"
	  "  default:\n"
	  "    fprintf(stderr, \"Illegal syscall: %%u\\n\", num);\n"
	  "    exit(1);\n"
	  "  }\n"
	  "}\n"
	  "\n"
	  "struct turbovm *run(struct turbovm *vm) {\n"
	  "  TVM_WORD *reg = vm->reg;\n"
	  "  while(1) {\n    switch(vm->instr) {\n");
  for(p = 0; p < size; p += 4) {
    emit_instruction(stream, code, size, &p);
  }
  fprintf(stream, "    default:\n"
	  "        /* whoops! value out of range! */\n"
	  "        fprintf(stderr, \"Invalid instruction pointer: %%u\\n\", vm->instr);"
	  "        exit(0x55);\n"
	  "        break;\n"
	  "    }\n  }\n}\n"
	  "\n"
	  "int main(int argc, char **argv) {\n"
	  "  struct turbovm vm;\n"
	  "  memset(&(vm.reg), 0, sizeof(vm.reg));\n"
	  "  vm.instr = 0;\n"
	  "  run(&vm);\n"
	  "  /* Should not be reached. */\n"
	  "  return 1;\n"
	  "}\n");
}

void compile(BYTE *code, WORD size) {
  compile_to_stream(stdout, code, size);
}

int main(int argc, char **argv) {
  int n = 0x10000;
  struct turbovm *vm;

  /* Initialize vm */
  vm = (struct turbovm*) malloc(sizeof(struct turbovm));
  if(!vm) {
    perror("malloc");
    exit(1);
  }
  memset(vm, 0, sizeof(struct turbovm));

  /* Load program */
  while(n == 0x10000) {
    vm->code = realloc(vm->code, vm->code_size + 0x10000);
    if(!vm->code) {
      perror("realloc");
      exit(1);
    }
    n = fread(&(vm->code[vm->code_size]), 1, 0x10000, stdin);
    if(ferror(stdin)) {
      perror("fread");
      exit(1);
    }
    vm->code_size += n;
  }

  /* Compile. */
  compile(vm->code, vm->code_size);

  return 0;
}
