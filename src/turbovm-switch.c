#include "turbovm.h"
#include "opcodes.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BYTE uint8_t
#define IMM_T uint16_t
#define IMMS_T int16_t
#define WORD TVM_WORD

#define OPCODE instr[0]
#define REG(N) reg[N]
#define REG1 REG(instr[1])
#define REG2 REG(instr[2])
#define REG3 REG(instr[3])
#define IMM (((IMM_T) (instr[2] << 8)) | ((IMM_T) instr[3]))
#define IMMS (((IMMS_T) (instr[2] << 8)) | ((IMMS_T) instr[3]))
#define IMPL(OP, CODE) case TVM_OP_ ## OP: CODE; NEXT
#define BINOP(OP, SYM) IMPL(OP, REG1 = REG2 SYM REG3)
#define BINOPI(OP, SYM) IMPL(OP, REG1 SYM ## = IMM)
#define BINOPIS(OP, SYM) IMPL(OP, REG1 SYM ## = IMMS)
#define NEXT instr += 4; break

struct turbovm {
  WORD reg[256];
  BYTE *code;
  WORD code_size;
};

static void syscall(struct turbovm *vm, IMM_T num) {
  WORD *reg = vm->reg;
  switch(num) {
  case 1:
    exit(REG(3) & 0xff);
    break;
  case 2:
    printf("%d\n", REG(3));
    break;
  default:
    fprintf(stderr, "Illegal syscall: %u\n", num);
    exit(1);
  }
}

static void run(struct turbovm *vm) {
  BYTE *instr = vm->code;
  WORD *reg = vm->reg;
  while(1) {
    switch(OPCODE) {
      BINOP(AND, &);
      BINOP(OR, |);
      BINOP(XOR, ^);
      IMPL(SH, REG1 = (REG3 < 0) ? REG2 >> -REG3 : REG2 << REG3);
      IMPL(SHS, REG1 = (REG3 < 0) ? REG2 >> -REG3 : REG2 << REG3);
      IMPL(ROT, REG1 = (REG3 < 0)
	   ? ((REG2 >> -REG3) | (REG2 << (TVM_WORDSIZE + REG3)))
	   : ((REG2 << REG3) | (REG2 >> (TVM_WORDSIZE - REG3))));
      IMPL(LB, REG1 = (WORD) *((BYTE *) (REG2 + REG3)));
      IMPL(LR, REG1 = REG2);
      IMPL(LW, REG1 = *((WORD *) (REG2 + REG3)));
      IMPL(STB, *((BYTE *) (REG2 + REG3)) = (BYTE) (REG1 & 0xff));
      IMPL(STW, *((WORD *) (REG2 + REG3)) = REG1);

      BINOPI(ANDI, &);
      BINOPI(ORI, |);
      BINOPI(XORI, ^);
      IMPL(SHI, REG1 = (IMM < 0) ? REG2 >> -IMM : REG2 << IMM);
      IMPL(ROTI, REG1 = (IMM < 0) 
	   ? ((REG2 >> -IMM) | (REG2 << (TVM_WORDSIZE + IMM)))
	   : ((REG2 << IMM) | (REG2 >> (TVM_WORDSIZE - IMM))));
      IMPL(LI, REG1 = IMM);
      IMPL(LUI, REG1 = IMM << 16);

      BINOP(ADD, +);
      BINOP(DIV, /);
      BINOP(DIVS, /);
      BINOP(MOD, %);
      BINOP(MODS, %);
      BINOP(MUL, *);
      BINOP(MULS, *);
      BINOP(SUB, -);

      BINOPI(ADDI, +);
      BINOPI(DIVI, /);
      BINOPIS(DIVIS, /);
      BINOPI(MODI, %);
      BINOPIS(MODIS, %);
      BINOPI(MULI, *);
      BINOPIS(MULIS, *);
      BINOPI(SUBI, -);

    case TVM_OP_CALL:
      REG1 = (WORD) instr + 4; instr = (BYTE*) REG2; break;
    case TVM_OP_GOTO:
      instr = (BYTE*) REG1; break;

    case TVM_OP_CALLI:
      REG1 = (WORD) instr + 4; instr += IMMS; break;
    case TVM_OP_GOTOI:
      instr += IMMS; break;
      IMPL(SYS, syscall(vm, IMM));

      IMPL(IFEQ, if(REG1 != REG2) instr += 4);
      IMPL(IFGE, if(REG1 < REG2) instr += 4);
      IMPL(IFGT, if(REG1 <= REG2) instr += 4);
      IMPL(IFLE, if(REG1 > REG2) instr += 4);
      IMPL(IFLT, if(REG1 >= REG2) instr += 4);
      IMPL(IFNE, if(REG1 == REG2) instr += 4);

    default:
      fprintf(stderr, "Illegal opcode %02x at %08x\n", OPCODE, (WORD) (instr - vm->code));
      exit(255);
    }
  }
}

int main(int argc, char **argv) {
  int n = 0x10000;
  struct turbovm *vm;

  /* Initialize vm */
  vm = (struct turbovm*) malloc(sizeof(struct turbovm));
  if(!vm) {
    perror("malloc");
    exit(1);
  }
  memset(vm, 0, sizeof(struct turbovm));

  /* Load program */
  while(n == 0x10000) {
    vm->code = realloc(vm->code, vm->code_size + 0x10000);
    if(!vm->code) {
      perror("realloc");
      exit(1);
    }
    n = fread(&(vm->code[vm->code_size]), 1, 0x10000, stdin);
    if(ferror(stdin)) {
      perror("fread");
      exit(1);
    }
    vm->code_size += n;
  }

  /* Run */
  run(vm);

  /* Should not be reached */
  return 1;
}
