#include "turbovm.h"
#include "opcodes.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define BYTE uint8_t
#define IMM_T int16_t
#define WORD TVM_WORD

#define OPCODE instr[0]
#define IMM (((IMM_T) (instr[2] << 8)) | ((IMM_T) instr[3]))

#define I(OP) case TVM_OP_ ## OP: \
  fprintf(stream, "L%010u: %s %d\n", \
    ip, strtolower(#OP), IMM); \
  break

#define R(OP) case TVM_OP_ ## OP: \
  fprintf(stream, "L%010u: %s r%u\n", \
    ip, strtolower(#OP), instr[1]); \
  break

#define RI(OP) case TVM_OP_ ## OP: \
  fprintf(stream, "L%010u: %s r%u %d\n", \
    ip, strtolower(#OP), instr[1], IMM); \
  break

#define RR(OP) case TVM_OP_ ## OP: \
  fprintf(stream, "L%010u: %s r%u r%u\n", \
    ip, strtolower(#OP), instr[1], instr[2]); \
  break

#define RRR(OP) case TVM_OP_ ## OP: \
  fprintf(stream, "L%010u: %s r%u r%u r%u\n", \
    ip, strtolower(#OP), instr[1], instr[2], instr[3]); \
  break

/* Holy crappy code Batman!
 * Make sure to strdup the result of this function before
 * calling it again!
 */
static char *strtolower(const char *str) {
  static char *result = NULL;
  int i, len = strlen(str);

  result = (char*) realloc(result, len + 1);
  if(result) {
    for(i = 0; i < len; i++) {
      result[i] = tolower(str[i]);
    }
    result[len] = 0;
  }

  return result;
}

void disassemble_to_stream(FILE *stream, BYTE *code, unsigned int code_size) {
  unsigned int ip = 0;
  BYTE *instr = code;
  while(ip < code_size) {
    switch(OPCODE) {
      RRR(AND);
      RRR(OR);
      RRR(XOR);
      RRR(SH);
      RRR(SHS);
      RRR(ROT);
      RRR(LB);
      RR(LR);
      RRR(LW);
      RRR(STB);
      RRR(STW);

      RI(ANDI);
      RI(ORI);
      RI(XORI);
      RI(SHI);
      RI(ROTI);
      RI(LI);
      RI(LUI);

      RRR(ADD);
      RRR(DIV);
      RRR(DIVS);
      RRR(MOD);
      RRR(MODS);
      RRR(MUL);
      RRR(MULS);
      RRR(SUB);

      RI(ADDI);
      RI(DIVI);
      RI(MODI);
      RI(MODIS);
      RI(MULI);
      RI(SUBI);

      RR(CALL);
      R(GOTO);

      RI(CALLI);
      I(GOTOI);
      I(SYS);

      RR(IFEQ);
      RR(IFGE);
      RR(IFGT);
      RR(IFLE);
      RR(IFLT);
      RR(IFNE);
    default:
      fprintf(stderr, "Illegal opcode %02x at %08x\n", OPCODE, ip);
      exit(255);
    }
    
    instr += 4;
    ip += 4;
  }
}

void disassemble(BYTE *code, unsigned int code_size) {
  disassemble_to_stream(stdout, code, code_size);
}

int main(int argc, char **argv) {
  int n = 0x10000, code_size = 0;
  BYTE *code = NULL;
  
  /* Load program */
  while(n == 0x10000) {
    code = realloc(code, code_size + 0x10000);
    if(!code) {
      perror("realloc");
      exit(1);
    }
    n = fread(&(code[code_size]), 1, 0x10000, stdin);
    if(ferror(stdin)) {
      perror("fread");
      exit(1);
    }
    code_size += n;
  }

  disassemble(code, code_size);
  
  return 0;
}
