#ifndef TURBOVM_H
#define TURBOVM_H

#include <inttypes.h>

#ifndef INLINE
#define INLINE inline
#endif

#define TVM_WORD int32_t
#define TVM_WORDSIZE 32

#endif /* ndef TURBOVM_H */
