#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include "opcodes.h"

#define ADDRESS_T unsigned
#define BYTE uint8_t
#define NEW(NAME, TYPE) TYPE *NAME = (TYPE *) malloc(sizeof(TYPE))

/** Instruction formats. */
#define FMT_I 1
#define FMT_R 2
#define FMT_RI 3
#define FMT_RR 4
#define FMT_RRR 5

/** A linked list, terminated by NULL.  Each element consists of a
 * pointer to some data, and a pointer to the next element.
 */
struct list {
  void *data;
  struct list *next;
};

/** Information about an instruction that needs to be patched up.
 * It contains the address at which the instruction resides,
 * the format of the instruction, and its operands.
 */
struct instruction {
  ADDRESS_T address;
  int format;
  int opcode, reg1;
};

/** Information about a label. It contains the label's name, its
 * address, and a list of instructions that need to be patched up once
 * the label's address is known, and a flag telling whether the label's
 * address is known.
 */
struct label {
  char *name;
  int known;
  ADDRESS_T address;
  struct list *instructions;
};

/** All labels in the program. */
static struct list *labels = NULL;

/** Address of next instruction. */
static ADDRESS_T current_address;

/** Code that has been emitted so far. */
static BYTE *code = NULL;

/** Size of code block. */
static ADDRESS_T code_size = 0;

/* Forward declarations, yay! */
static BYTE *insert_word(ADDRESS_T address, uint32_t word);
static struct instruction *make_i_instruction(ADDRESS_T address, int opcode);
static struct instruction *make_ri_instruction(ADDRESS_T address, int opcode, int reg);

/** Add an instruction to a label. */
static struct instruction *add_instruction_to_label(struct label *x, struct instruction *y) {
  NEW(z, struct list);
  if(z) {
    z->data = y;
    z->next = x->instructions;
    x->instructions = z;
    return y;
  } else return NULL;
}

/** Add a new i-instruction to a label. */
static struct instruction *add_i_instruction_to_label(struct label *x, ADDRESS_T address, int opcode) {
  struct instruction *y = make_i_instruction(address, opcode);
  if(y) {
    if(add_instruction_to_label(x, y)) return y;
    else {
      free(y);
      return NULL;
    }
  } else return NULL;
}

/** Add a new ri-instruction to a label. */
static struct instruction *add_ri_instruction_to_label(struct label *x, ADDRESS_T address, int opcode, int reg) {
  struct instruction *y = make_ri_instruction(address, opcode, reg);
  if(y) {
    if(add_instruction_to_label(x, y)) return y;
    else {
      free(y);
      return NULL;
    }
  } else return NULL;
}

/** Insert an instruction of i-format at a given address. */
static void insert_i(ADDRESS_T address, int opcode, int imm) {
  uint32_t word = 0;
  BYTE *p = (BYTE*) &word;
  p[0] = (BYTE) opcode;
  p[2] = (BYTE) (imm >> 8);
  p[3] = (BYTE) (imm & 0xff);
  insert_word(address, word);
}

/** Insert an instruction of r-format at a given address. */
static void insert_r(ADDRESS_T address, int opcode, int reg) {
  uint32_t word = 0;
  BYTE *p = (BYTE*) &word;
  p[0] = (BYTE) opcode;
  p[1] = (BYTE) reg;
  insert_word(address, word);
}

/** Insert an instruction of ri-format at a given address. */
static void insert_ri(ADDRESS_T address, int opcode, int reg, int imm) {
  uint32_t word = 0;
  BYTE *p = (BYTE*) &word;
  p[0] = (BYTE) opcode;
  p[1] = (BYTE) reg;
  p[2] = (BYTE) (imm >> 8);
  p[3] = (BYTE) (imm & 0xff);
  insert_word(address, word);
}

/** Insert an instruction of rr-format at a given address. */
static void insert_rr(ADDRESS_T address, int opcode, int reg1, int reg2) {
  uint32_t word = 0;
  BYTE *p = (BYTE*) &word;
  p[0] = (BYTE) opcode;
  p[1] = (BYTE) reg1;
  p[2] = (BYTE) reg2;
  insert_word(address, word);
}

/** Insert an instruction of rrr-format at a given address. */
static void insert_rrr(ADDRESS_T address, int opcode, int reg1, int reg2, int reg3) {
  uint32_t word = 0;
  BYTE *p = (BYTE*) &word;
  p[0] = (BYTE) opcode;
  p[1] = (BYTE) reg1;
  p[2] = (BYTE) reg2;
  p[3] = (BYTE) reg3;
  insert_word(address, word);
}


/** Find a label by name.
 * Returns the label if found, NULL if not found.
 */
static struct label *find_label(char *name) {
  struct label *y;
  struct list *x = labels;
  while(x) {
    y = x->data;
    if(!strcmp(name, y->name)) return y;
    x = x->next;
  }
  return NULL;
}

/** Free a list. */
void free_list(struct list *x) {
  struct list *next;
  while(x) {
    free(x->data);
    next = x->next;
    free(x);
    x = next;
  }
}

/** Insert a word into the code. */
static BYTE *insert_word(ADDRESS_T address, uint32_t word) {
  ADDRESS_T newsize;
  if(address + 4 > code_size) {
    newsize = (address / 0x1000 + 1) * 0x1000;
    code = realloc(code, newsize);
    if(!code) return NULL;
    code_size = newsize;
  }

  memcpy(&code[address], &word, 4);
  return code;
}

/** Make an i-format instruction. */
static struct instruction *make_i_instruction(ADDRESS_T address, int opcode) {
  NEW(x, struct instruction);
  if(x) {
    x->address = address;
    x->format = FMT_I;
    x->opcode = opcode;
  }
  return x;
}

/** Make a new label. */
static struct label *make_label(char *name, int known, ADDRESS_T address, struct list *instructions) {
  NEW(x, struct label);
  if(x) {
    x->name = name;
    x->known = known;
    x->address = address;
    x->instructions = instructions;
  }
  return x;
}

/** Make a new label with a known address. */
static struct label *make_known_label(char *name, ADDRESS_T address) {
  return make_label(name, 1, address, NULL);
}

/** Make a new label without a known address, but with an instruction to be patched up. */
static struct label *make_label_with_instruction(char *name, struct instruction *instruction) {
  struct label *y;
  NEW(x, struct list);
  if(x) {
    x->data = instruction;
    x->next = NULL;
    y = make_label(name, 0, 0, x);
    if(y) return y;
    else {
      free(x);
      return NULL;
    }
  } else return NULL;
}

/** Make a new uknonwn label with an i-instruction to be patched. */
static struct label *make_label_with_i_instruction(char *name, ADDRESS_T address, int opcode) {
  struct label *y;
  struct instruction *x = make_i_instruction(address, opcode);
  if(x) {
    y = make_label_with_instruction(name, x);
    if(y) return y;
    else {
      free(x);
      return NULL;
    }
  } else return NULL;
}

/** Make a new uknonwn label with an ri-instruction to be patched. */
static struct label *make_label_with_ri_instruction(char *name, ADDRESS_T address, int opcode, int reg) {
  struct label *y;
  struct instruction *x = make_ri_instruction(address, opcode, reg);
  if(x) {
    y = make_label_with_instruction(name, x);
    if(y) return y;
    else {
      free(x);
      return NULL;
    }
  } else return NULL;
}

/** Make an ri-format instruction. */
static struct instruction *make_ri_instruction(ADDRESS_T address, int opcode, int reg) {
  NEW(x, struct instruction);
  if(x) {
    x->address = address;
    x->format = FMT_RI;
    x->opcode = opcode;
    x->reg1 = reg;
  }
  return x;
}

/** Patch an instruction. */
void patch_instruction(struct instruction *x, ADDRESS_T address) {
  switch(x->format) {
  case FMT_I:
    switch(x->opcode) {
    case TVM_OP_GOTOI:
      insert_i(x->address, x->opcode, address - x->address);
      break;
    default:
      fprintf(stderr, "Don't know how to patch instruction with opcode %02x\n", x->opcode);
    }
    break;
  case FMT_RI:
    switch(x->opcode) {
    case TVM_OP_CALLI:
      insert_ri(x->address, x->opcode, x->reg1, address - x->address);
      break;
    default:
      fprintf(stderr, "Don't know how to patch instruction with opcode %02x\n", x->opcode);
    }
    break;
  default:
    fprintf(stderr, "Don't know how to patch instruction of format %u\n", x->format);
  }
}

/** Patch instructions. */
void patch_instructions(struct list *x, ADDRESS_T address) {
  while(x) {
    patch_instruction((struct instruction*) x->data, address);
    x = x->next;
  }
}

/** Register a label. */
struct list *register_label(struct label *x) {
  NEW(y, struct list);
  if(y) {
    y->data = x;
    y->next = labels;
    labels = y;
  }
  return y;
}

/** Create a label with an i-instruction to be patched and register the label. */
struct list *register_label_with_i_instruction(char *name, ADDRESS_T address, int opcode) {
  struct list *y;
  struct label *x = make_label_with_i_instruction(name, address, opcode);
  if(x) {
    y = register_label(x);
    if(y) return y;
    else {
      free(x);
      return NULL;
    }
  } else return NULL;
}
    
/** Create a label with an ri-instruction to be patched and register the label. */
struct list *register_label_with_ri_instruction(char *name, ADDRESS_T address, int opcode, int reg) {
  struct list *y;
  struct label *x = make_label_with_ri_instruction(name, address, opcode, reg);
  if(x) {
    y = register_label(x);
    if(y) return y;
    else {
      free(x);
      return NULL;
    }
  } else return NULL;
}
    
/** Register a label's address.
 * This will create and register the label if it didn't exist yet,
 * and patch up any instructions as necessary.
 */
struct label *register_label_address(char *name, ADDRESS_T address) {
  struct label *x = find_label(name);
  if(x) {
    x->address = address;
    x->known = 1;
    patch_instructions(x->instructions, address);
    free_list(x->instructions);
    x->instructions = NULL;
  } else {
    x = make_known_label(name, address);
    if(x) {
      register_label(x);
    }
  }
  return x;
}

static void emit_i(int op, int imm) {
  insert_i(current_address, op, imm);
  current_address += 4;
}

static void emit_i_ref(int op, char *reference) {
  struct label *label = find_label(reference);
  if(label) {
    /* Label found, do we know its address yet? */
    if(label->known) {
      /* Yes. Good! Emit instruction. */
      emit_i(op, label->address - current_address);
    } else {
      /* No. Too bad. Schedule instruction for patching. */
      add_i_instruction_to_label(label, current_address, op);
      current_address += 4;
    }
  } else {
    /* Label not found. */
    register_label_with_i_instruction(reference, current_address, op);
    current_address += 4;
  }
}

/** Insert an r-instruction at the current address. */
static void emit_r(int op, int reg) {
  insert_r(current_address, op, reg);
  current_address += 4;
}

/** Insert an ri-instruction at the current address. */
static void emit_ri(int op, int reg, int imm) {
  insert_ri(current_address, op, reg, imm);
  current_address += 4;
}

/** Insert an ri-instruction that refers to a label at the current address.
 * If the address of the label is not known yet, the instruction is added
 * to the label's patch list.
 */
static void emit_ri_ref(int op, int reg, char *reference) {
  struct label *label = find_label(reference);
  if(label) {
    /* Label found, do we know its address yet? */
    if(label->known) {
      /* Yes. Good! Emit instruction. */
      emit_ri(op, reg, label->address - current_address);
    } else {
      /* No. Too bad. Schedule instruction for patching. */
      add_ri_instruction_to_label(label, current_address, reg, op);
      current_address += 4;
    }
  } else {
    /* Label not found. */
    register_label_with_ri_instruction(reference, current_address, op, reg);
    current_address += 4;
  }
}

/** Insert an rr-instruction at the current address. */
static void emit_rr(int op, int reg1, int reg2) {
  insert_rr(current_address, op, reg1, reg2);
  current_address += 4;
}

/** Insert an rrr-instruction at the current address. */
static void emit_rrr(int op, int reg1, int reg2, int reg3) {
  insert_rrr(current_address, op, reg1, reg2, reg3);
  current_address += 4;
}

#include "turbovm-asm.yy.c"
#include "turbovm-asm.tab.c"

int main(int argc, char **argv) {
  yyparse();
  fwrite(code, 1, current_address, stdout);
  return 0;
}
